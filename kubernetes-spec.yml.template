apiVersion: apps/v1beta2
kind: Deployment
metadata:
  name: devices
  namespace: {{ KUBE_NAMESPACE }}
spec:
  replicas: 1
  selector:
    matchLabels:
      app: devices
  template:
    metadata:
      labels:
        app: devices
    spec:
      containers:
      - name: devices
        image: {{ DOCKER_REGISTRY }}/vxlabs/iot-devices:{{ COMMIT_HASH }}
        env:
          - name: KAFKA_BROKERS
            value: {{ KAFKA_BROKERS }}
          - name: APPROLE_ID
            value: {{ APPROLE_ID }}
          - name: APPROLE_SECRET
            value: {{ APPROLE_SECRET }}
        ports:
        - containerPort: 8006

---
apiVersion: v1
kind: Service
metadata:
  name: devices
  namespace: {{ KUBE_NAMESPACE }}
spec:
  ports:
  - name: http
    targetPort: 8006
    port: 80
  selector:
    app: devices

---
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: devices
  namespace: {{ KUBE_NAMESPACE }}
  annotations:
    kubernetes.io/ingress.class: nginx
    kubernetes.io/tls-acme: "true"
spec:
  tls:
    - hosts:
        - {{ ENVIRONMENT_PUBLIC_NAME }}
      secretName: devices-tls
  rules:
  - host: {{ ENVIRONMENT_PUBLIC_NAME }}
    http:
      paths:
      - path: /
        backend:
          serviceName: devices
          servicePort: http

