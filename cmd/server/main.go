package main

import (
	"context"
	api "github.com/vx-labs/go-rest-api/client"
	"github.com/vx-labs/identity-api/authentication"
	"github.com/vx-labs/iot-devices/resources"
)

func main() {
	logger := api.NewLogger()

	ctx := context.Background()

	auth, err := authentication.NewClient(ctx, logger.WithField("source", "authentication_client"))
	if err != nil {
		logger.Fatalf(err.Error())
	}

	server, err := auth.NewResourceServer(ctx, logger.WithField("source", "service"), "iot", "v1")
	if err != nil {
		logger.Fatalf(err.Error())
	}
	server.AddResource("/", resources.DeviceHandler())
	logger.Error(server.ListenAndServe(ctx, 8006))
}
