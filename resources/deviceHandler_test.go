package resources

import (
	"testing"
	"github.com/docker/distribution/uuid"
	"github.com/stretchr/testify/assert"
	"time"
	"github.com/vx-labs/identity-api/messages"
	"github.com/vx-labs/identity-api/filter"
)

func TestDeviceHandler_Events_HomeDeleted(t *testing.T) {
	a := DeviceHandler()
	id := uuid.Generate().String()
	homeId := uuid.Generate().String()
	ownerId := uuid.Generate().String()
	d := &Device{
		Id: id,
		Owner: ownerId,
		HomeId: homeId,
		Name:   "mock",
	}
	if assert.Nil(t, a.Store().Insert(d)) {
		go func() {
			a.CommandsSubscriptions()["delete_resource"](&messages.ResourceEvent{
				Id:      homeId,
				Kind:    "home",
				Service: "iot",
				Owner:   ownerId,
			})
		}()
		select {
		case ev := <-a.Commands():
			l, _ := a.Store().List(filter.NewResourceFilter().Build())
			assert.Equal(t, 1, len(l))
			assert.Equal(t, "delete_resource", ev.(*messages.ResourceEvent).Type)
			assert.Equal(t, "device", ev.(*messages.ResourceEvent).Kind)
			assert.Equal(t, id, ev.(*messages.ResourceEvent).Id)
		case <-time.After(10 * time.Millisecond):
			t.Error("event timed out")
		}
	}
}
