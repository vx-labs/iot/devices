package resources

import (
	"testing"
	"github.com/stretchr/testify/assert"
)

func Test_Device(t *testing.T) {
	h := Device{}
	assert.NotNil(t, h)
}

func Test_Device_Validate(t *testing.T) {
	h := &Device{}
	assert.False(t, h.Validate())
	h = &Device{
		Name: "test",
	}
	assert.True(t, h.Validate())
}
