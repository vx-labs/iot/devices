package resources

import (
	"github.com/vx-labs/identity-types/server"
	"github.com/vx-labs/go-rest-api"
	"github.com/vx-labs/identity-api/store"
	"encoding/json"
	"github.com/vx-labs/identity-api/messages"
	"github.com/vx-labs/identity-api/filter"
)

type deviceHandler struct {
	state    server.StateStore
	events   chan server.Event
	commands chan server.Event
}

func DeviceHandler() server.ResourceHandler {
	h := &deviceHandler{
		events:   make(chan server.Event),
		commands: make(chan server.Event),
	}
	h.state = store.NewMemoryStoreFromResource(h.Schema())
	return h
}

func (h *deviceHandler) Store() server.StateStore {
	return h.state
}
func (h *deviceHandler) Schema() server.Resource {
	return &Device{}
}
func (h *deviceHandler) Events() chan server.Event {
	return h.events
}
func (h *deviceHandler) Commands() chan server.Event {
	return h.commands
}
func (h *deviceHandler) EventsSubscriptions() map[string]server.EventHandler {
	return map[string]server.EventHandler{}
}
func (h *deviceHandler) CommandsSubscriptions() map[string]server.EventHandler {
	return map[string]server.EventHandler{
		"delete_resource": func(ev server.Event) {
			b, err := ev.Encode()
			if err != nil {
				return
			}
			resourceEvent := messages.ResourceEvent{}
			err = json.Unmarshal(b, &resourceEvent)
			if err != nil {
				return
			}
			if resourceEvent.Kind == "home" {
				devices, err := h.state.GetAll("home_id", resourceEvent.Id, filter.NewResourceFilter().AddOwner(resourceEvent.Owner).Build())
				if err != nil {
					return
				}
				for _, dev := range devices {
					h.Commands() <- messages.NewResourceDeleteCommand(resourceEvent.RequestId, resourceEvent.Service, h.Grammar().Singular, resourceEvent.Owner, dev.UniqueId())
				}
			}
		},
	}
}
func (h *deviceHandler) Grammar() api.Grammar {
	return api.Grammar{
		Plural:   "devices",
		Singular: "device",
	}
}
