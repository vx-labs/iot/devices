package resources

import (
	"time"
	"github.com/vx-labs/identity-types/server"
	"encoding/json"
)

type Device struct {
	Id      string    `json:"id" indexed:"uuid,unique"`
	Owner   string    `json:"owner" indexed:"string"`
	Created time.Time `json:"created"`
	Updated time.Time `json:"updated"`
	Name    string    `json:"name" indexed:"string"`
	HomeId  string    `json:"home_id" indexed:"uuid"`
	CertId  string    `json:"cert_id"`
}

func (h *Device) UniqueId() string {
	return h.Id
}
func (h *Device) OwnerId() string {
	return h.Owner
}

func (h *Device) Validate() bool {
	return h.Name != ""
}
func (h *Device) WithOwner(owner string) server.Resource {
	c := *h
	c.Owner = owner
	return &c
}

func (h *Device) WithId(id string) server.Resource {
	c := *h
	c.Id = id
	return &c
}

func (h *Device) WithUpdated(t time.Time) server.Resource {
	c := *h
	c.Updated = t
	return &c
}
func (h *Device) WithCreated(t time.Time) server.Resource {
	c := *h
	c.Created = t
	return &c
}

func (h *Device) FromJSON(b []byte) error {
	return json.Unmarshal(b, h)
}
func (h *Device) ToJSON() ([]byte, error) {
	return json.Marshal(h)
}
