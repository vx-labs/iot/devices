FROM alpine
EXPOSE 8006
ENTRYPOINT ["/usr/bin/server"]
RUN apk -U add ca-certificates && \
    rm -rf /var/cache/apk/*
COPY release/server /usr/bin/server

